# 2HDMC
2HDMC is a general-purpose calculator for the two-Higgs doublet model. It allows parametrization of the Higgs potential in many different ways, convenient specification of generic Yukawa sectors, the evaluation of decay widths (including higher-order QCD corrections), theoretical constraints and much more.

- Main documentation page: [https://2hdmc.hepforge.org](https://2hdmc.hepforge.org)
- Working example: [Calculators42HDM](https://github.com/kjaffel/Calculators42HDM)
- Calculate [THDM-II mass sepctrum online](https://flexiblesusy.hepforge.org/online/online.php#THDMII)

