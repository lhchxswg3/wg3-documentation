# How to Contribute ? 

## To work-in-progress 

* Explore ongoing efforts and volunteer for topics of interest: [https://gitlab.cern.ch/lhchxswg3/2HDM/-/boards](https://gitlab.cern.ch/lhchxswg3/2HDM/-/boards)


## To the documentation 
### Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

### Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.
