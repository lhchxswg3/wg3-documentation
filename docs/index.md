# Welcome to LHC Higgs Working Group

<!--
![Alt text](img/Logo_Large.png)
-->

<img src="img/Logo_Large.png" alt="Alt text" width="300"/>

The LHC Higgs Working Group (LHCHWG) advises experimental endeavors on theoretical forecasts and methodologies pertaining to Higgs boson studies across ongoing and forthcoming LHC operations. Broader topics, such as global Effective Field Theories or investigations into Dark Matter models, are also explored collaboratively by LHCHWG alongside other comprehensive initiatives within the LHC community.

Originally established in January 2010 as the "LHC Higgs Cross Section Working Group," its primary objective was to foster consensus within the LHC community concerning cross sections, branching ratios, and pertinent pseudo-observables for both Standard Model (SM) and Minimal Supersymmetric Standard Model (MSSM) Higgs bosons. In spring 2012, the group underwent restructuring, incorporating new subgroups to delve into Higgs properties, measurements, and Beyond the Standard Model (BSM) extensions. Further reorganization occurred in 2014, introducing the Theory Advisory Committee (TAC) and establishing a partnership with CERN for local support. The TAC consistently includes a permanent CERN theory group member. Oversight of group activities rests with the Steering Committee (SC), comprising two representatives from each experiment and four from the theory community. ATLAS or CMS SC members are nominated by their respective experiments, while theory community representatives are appointed by the TAC, in coordination with the SC.

Over time, the group's scope expanded significantly, leading to the decision in November 2020 to rename it the "LHC Higgs Working Group." Four CERN Yellow Reports have been produced, including "Inclusive Observables," "Differential Distributions," "Higgs Properties," and "Deciphering the Nature of the Higgs Sector," enhancing the ability to compare and integrate Higgs results throughout the LHC's physics endeavors.

The group is structured into four working groups:

- WG1: Higgs cross-sections and branching ratios
- WG2: Higgs properties
- WG3: Beyond the Standard Model (BSM) Higgs
	- Subgroups
		- bbH/bH 
		- [Extended Higgs Sector](https://lhchxswg3.docs.cern.ch/wg3_subgroup_extended-higgs-sector/) 
		- MSSM
		- NMSSM 
		- [Exotic Higgs Decays](http://exotichiggs.physics.sunysb.edu/web/wopr/?page_id=68)
- WG4: HH and multi-Higgs

To participate in the working group, prospective members are encouraged to contact the conveners via email and subscribe to the relevant mailing lists. Several activities intersect multiple groups, necessitating collaboration across working groups.

