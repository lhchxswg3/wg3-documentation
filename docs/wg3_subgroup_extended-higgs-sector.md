# WG3: Extended Higgs Sector (subgroup)

- **Supgroup Conveners:** 
    - **ATLAS:** Lidija Zivkovic, Belgrade IP (2019) | Nikos Rompotis, Liverpool (01/2021)
    - **CMS:** Alexandra Carvalho, PKU (01/2024) | Khawla Jaffel, USTC (03/2024)
    - **Theory:** Tania Robens, Zagreb (02/2021) | Rui Santos, ISEL/CFTC, Lisbon (2015)
- **[TWiki](https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHWG3)** 
- **WG3 mailing list:** [lhc-higgs-bsm@SPAMNOTcern.ch](mailto:lhc-higgs-bsm@SPAMNOTcern.ch)
- **Extended Higgs Sector subgroup convenors mailing list:** [lhc-higgs-bsm-convener@SPAMNOTcern.ch](nailto:lhc-higgs-bsm-convener@SPAMNOTcern.ch)
- **Past & Upcomings Extended Higgs Sector subgroup meetings:** [indico](https://indico.cern.ch/category/5849/)
