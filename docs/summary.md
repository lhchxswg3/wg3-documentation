# Extended Higgs Sector

## Summary of 2HDM+S searches at 13 TeV (Run 2)

This model is an extension of the Two Higgs Doublet Model (2HDM) with an additional EWK scalar singlet. The Next-To-Minimal-Supersymmetric-Standard-Model (NMSSM) is a particular case of 2HDM+S type-2.

- Plots can be found on the [Twiki](https://twiki.cern.ch/twiki/bin/view/CMSPublic/Summary2HDMSRun2).

## BSM particles decaying to Higgs, top and Gauge bosons (B2G) group, searches at 13 TeV (Run 2)

The plots covers models of new physics featuring heavy standard model particles in the final state (W, Z, H, boosted top or 'resonances X→top+Y’). This includes searches for diboson resonances, heavy partners of the top quark with vector-like properties (VLQ) as well as heavy resonances, such as W' and Z' resonances, and heavy neutral or charged Higgs bosons. 
In many of these searches the final state particles can have significant Lorentz boosts, so that their individual decay products often overlap and merge. 

- Plots can be found on the [Twiki](https://twiki.cern.ch/twiki/bin/view/CMSPublic/PhysicsResultsB2G#B2G_Summary_Plots).
