# Contact

<img src="img/mattermostlogo.png" alt="Alt text" width="300"/>

The Mattermost team "LHC Higgs WG" and its associated channels serve as platforms for informal communication and discussions. Anyone can join the working group of these Mattermost teams by following the provided links.

- [WG1](https://mattermost.web.cern.ch/lhc-higgs-wg/channels/wg1-higgs-xsbr)
- [WG2](https://mattermost.web.cern.ch/lhc-higgs-wg/channels/wg2-higgs-properties)
- [WG3](https://mattermost.web.cern.ch/lhc-higgs-wg/channels/wg3-bsm-higgs)
- [WG4/LHC-HH](https://mattermost.web.cern.ch/lhc-higgs-wg/channels/wg3-session-2021)

## WG3 Subgroups
-------------

### bbH/bH 
  - **ATLAS:** Tim Barklow, SLAC (01/2021)
  - **CMS:** Chayanit Asawatangtrakuldee (Chulalongkorn U.) (07/2021)
  - **Theory:** Michael Spira, PSI (08/2014) | Theory: Marius Wiesemann, MPI Munich (09/2014)

### Extended Higgs Sector 
  - **ATLAS:** Lidija Zivkovic, Belgrade IP (2019) | Nikos Rompotis, Liverpool (01/2021)
  - **CMS:** Alexandra Carvalho, PKU (01/2024) |  Khawla Jaffel, USTC (03/2024)
  - **Theory:** Tania Robens, Zagreb (02/2021) | Rui Santos, ISEL/CFTC, Lisbon (2015)

### MSSM
  - **ATLAS:** Timothy Barklow, SLAC (2019)
  - **CMS:** Afiq Anuar, DESY (07/2021)
  - **Theory:** Emanuele Bagnaschi, CERN (06/2020) | Michael Spira, PSI (04/2012)

### NMSSM 
  - **ATLAS:** Nikos Rompotis, Liverpool
  - **CMS:** Daniel Winterbottom, Imperial (07/2021)
  - **Theory:** Ulrich Ellwanger, LPT, Orsay (2015) | Margarete Mühlleitner, KIT | Nausheen Shah, Wayne State (11/2019)

### Exotic Higgs Decays 
  - **ATLAS:** Verena Martinez, UMass (01/2021)
  - **CMS:** Fengwangdong Zhang, UCDavis (01/2024)
  - **LHCb:** Carlos Vazquez Sierra, USC
  - **Theory:** Brian Shuve, Harvey Mudd (01/2020) | Matthias König, TU Munich (01/2020)
