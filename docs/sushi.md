# SusHi
SusHi (Supersymmetric Higgs) is a Fortran code, which calculates Higgs cross sections
in gluon fusion and bottom-quark annihilation at hadron colliders in the SM,
the 2HDM, the MSSM and the NMSSM. Apart from inclusive cross sections up to
N3LO QCD, differential cross sections with respect to the Higgs' transverse
momentum and (pseudo)rapidity can be calculated. In case of gluon fusion,
SusHi contains NLO QCD contributions from the third family of quarks and
squarks, N3LO corrections due to top-quarks, approximate NNLO corrections
due to top squarks and electro-weak effects. Dimension-5 operators can be taken
into account. It supports various renormalization schemes
for the sbottom sector and the bottom Yukawa coupling, as well as resummation effects of
higher order tan(beta)-enhanced sbottom contributions. SusHi provides a link to FeynHiggs
for the calculation of Higgs masses in the MSSM and to 2HDMC for the 2HDM.

- Main documentation page: [https://sushi.hepforge.org](https://sushi.hepforge.org)
- SusHi online producing total cross sections and results for the renormalization scale dependence for (SM, 2HDM, MSSM, NMSSM, DIM5) models: [https://sushi.hepforge.org/online/](https://sushi.hepforge.org/online/)
