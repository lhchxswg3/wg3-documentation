# More of HEPForge Projects

- Other HEP tools can be found in: [https://www.hepforge.org/projects](https://sushi.hepforge.org/online/)
- You can run an online spectrum generator for various models (NMSSM, THDM, etc.) with FlexibleSUSY:
    - [https://flexiblesusy.hepforge.org/online/online.php](https://flexiblesusy.hepforge.org/online/online.php)

