# News and announcements 

## Organized by WG3 

- 

## Related to WG3
### Extended Scalar Sectors From All Angles 

- Oct 21 – 25, 2024. CERN, Europe/Zurich timezone
- Indico: https://indico.cern.ch/event/1376030/overview

<div style="background-color: lightgrey; padding: 10px; color: black;">

In this workshop, we plan to investigate new physics models from a large variety of viewpoints and perspectives.

After the discovery of a scalar boson that currently complies with the predictions for the Higgs boson of the Standard Model (SM), particle physics has entered an exciting era.
To date, the LHC experiments have confirmed predicted properties of such a boson, in particular the mass as well as couplings to other SM final states, to great precision. However, both theoretical and experimental accuracy still leave room for new physics models.
We here plan to focus on models with extended scalar sectors. Depending on the specific model, such scenarios can help to address additional questions that cannot be answered by the SM alone.
This concise workshop will bring together theorists and experimentalists to discuss current status and future perspectives for the discovery of such new physics scenarios.

</div>

----
